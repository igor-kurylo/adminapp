import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class BaseApi {

    private baseUrl = 'http://localhost:3000/';

    constructor(public http: HttpClient) {}

    /**
     * get url
     * @param {string} url
     * @returns {string}
     */
    private getUrl(url: string = ''): string {
        return this.baseUrl + url;
    }

    /**
     * function for method GET
     * @param {string} url
     * @returns {Observable<any>}
     */
    public get(url: string = ''): Observable<any> {
        return this.http.get(this.getUrl(url));
    }

    /**
     * function for method POST
     * @param {string} url
     * @param data
     * @returns {Observable<any>}
     */
    public post(url: string = '', data: any = {}): Observable<any> {
        return this.http.post(this.getUrl(url), data);
    }

    /**
     * function for method PUT
     * @param {string} url
     * @param data
     * @returns {Observable<any>}
     */
    public put(url: string = '', data: any = {}): Observable<any> {
        return this.http.put(this.getUrl(url), data);
    }

    /**
     * function for method DELETE
     * @param {string} url
     * @returns {Observable<any>}
     */
    public delete(url: string = ''): Observable<any> {
        return this.http.delete(this.getUrl(url));
    }

}