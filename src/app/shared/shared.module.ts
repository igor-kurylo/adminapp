import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {CommonMethodsService} from './services/common-methods.service';

@NgModule({
    imports: [
        ReactiveFormsModule,
        FormsModule,
        NgxChartsModule
    ],
    exports: [
        ReactiveFormsModule,
        FormsModule,
        NgxChartsModule
    ],
    declarations: [NotFoundComponent],
    providers: [CommonMethodsService]
})
export class SharedModule {
}
