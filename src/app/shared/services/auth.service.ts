import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

    constructor(private router: Router) {
    }

    private isAuthenticated = false;

    /**
     * login
     */
    login() {
        this.isAuthenticated = true;
    }

    /**
     * logout
     */
    logout() {
        this.isAuthenticated = false;
        window.sessionStorage.clear();
    }

    /**
     * login test
     * @returns {boolean}
     */
    isLoggedIn(): boolean {
        if (!this.isAuthenticated) {
            this.router.navigate(['/login']);
        }
        return this.isAuthenticated;
    }

}