import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseApi} from '../core/base-api';

@Injectable()
export class UserService extends BaseApi {

    constructor(public http: HttpClient) {
        super(http);
    }

    /**
     * get user by ID
     * @param {string} email
     * @returns {Observable<any>}
     */
    getUserById(email: string) {
        return this.get(`users?email=${email}`);
    }

    /**
     * add new user in DB
     * @param user
     * @returns {Observable<any>}
     */
    addNewUser(user) {
        return this.post('users', user);
    }

}