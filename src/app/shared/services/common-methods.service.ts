export class CommonMethodsService {
    /**
     * hide information message
     * @param {{type: string, message: string}} message
     */
    hideMessage(message) {
        setTimeout(() => {
           message.text = '';
        }, 3000);
    }

}