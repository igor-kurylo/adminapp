import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../shared/services/user.service';
import {Router} from '@angular/router';
import * as md5 from 'md5';
import {Message} from '../../shared/models/message.model';
import {CommonMethodsService} from '../../shared/services/common-methods.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    form: FormGroup;
    message: Message;

    constructor(private userService: UserService,
                private router: Router,
                private commonMethodsService: CommonMethodsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.message = new Message('danger', '');

        this.form = this.formBuilder.group({
            'email': ['', [Validators.required, Validators.email]],
            'password': ['', [Validators.required, Validators.minLength(6)]],
            'name': ['', Validators.required],
            'agree': ['', Validators.requiredTrue]
        });
    }

    /**
     * registration user and show message
     */
    onSubmit(): void {
        const {email, name} = this.form.value;
        const user = {
            name,
            surname: '',
            email,
            phone: '',
            password: md5(this.form.value.password)
        };
        this.userService.getUserById(email).takeUntil(this.destroy$).subscribe((data: any[]) => {
            if (data.length === 0) {
                this.userService.addNewUser(user).takeUntil(this.destroy$).subscribe(() => {
                    this.router.navigate(['/login'], {
                        queryParams: {
                            nowCanLogin: true
                        }
                    });
                });
            } else {
                this.message.text = 'The email is busy.';
                this.commonMethodsService.hideMessage(this.message);
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
