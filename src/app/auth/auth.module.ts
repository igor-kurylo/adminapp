import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserService} from '../shared/services/user.service';
import {AuthService} from '../shared/services/auth.service';
import {AuthRoutingModule} from './auth-routing.module';
import {SharedModule} from '../shared/shared.module';

import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AuthComponent} from './auth.component';


@NgModule({
    declarations: [
        LoginComponent,
        RegistrationComponent,
        AuthComponent
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        SharedModule
    ],
    providers: [
        UserService,
        AuthService
    ]
})
export class AuthModule {
}
