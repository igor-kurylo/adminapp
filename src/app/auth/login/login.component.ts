import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import * as md5 from 'md5';

import {UserService} from '../../shared/services/user.service';
import {AuthService} from '../../shared/services/auth.service';

import {User} from '../../shared/models/user.model';
import {Message} from '../../shared/models/message.model';
import {CommonMethodsService} from '../../shared/services/common-methods.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    form: FormGroup;
    message: Message;

    constructor(private userService: UserService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
                private commonMethodsService: CommonMethodsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.message = new Message('danger', '');

        this.form = this.formBuilder.group({
            'email': ['', [Validators.required, Validators.email]],
            'password': ['', [Validators.required, Validators.minLength(6)]]
        });
        // show message after successful registration
        this.route.queryParams.takeUntil(this.destroy$).subscribe((params: Params) => {
            if (params['nowCanLogin']) {
                this.message.text = 'Now you can come in system';
                this.message.type = 'success';
                this.commonMethodsService.hideMessage(this.message);
            }
        });
    }

    /**
     * login, set session storage and show message
     */
    onSubmit(): void {
        const formData = this.form.value;
        this.userService.getUserById(formData.email).takeUntil(this.destroy$).subscribe((data: User[]) => {
            if (data.length !== 0) {
                if (md5(formData.password) === data[0].password) {
                    this.message.text = '';
                    const userStorageData = {
                        name: data[0]['name'],
                        surname: data[0]['surname'],
                        email: data[0]['email'],
                        phone: data[0]['phone'],
                        password: data[0]['password'],
                        id: data[0]['id']
                    };
                    window.sessionStorage.setItem('login', JSON.stringify(userStorageData));
                    this.authService.login();
                    this.router.navigate(['/system', 'objects-list']);
                } else {
                    this.message.type = 'danger';
                    this.message.text = 'wrong password';
                    this.commonMethodsService.hideMessage(this.message);
                }
            } else {
                this.message.type = 'danger';
                this.message.text = 'wrong email';
                this.commonMethodsService.hideMessage(this.message);
            }
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
