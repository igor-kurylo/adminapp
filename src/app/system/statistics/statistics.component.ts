import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UsersService} from '../shared/services/users.service';
import {ObjectsService} from '../shared/services/objects.service';
import {CategoriesService} from '../shared/services/categories.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    view: any[] = [700, 400];
    dataUser = [];
    dataCat = [];

    allUsers;
    allObjects;
    allCategories;
    isLoad = false;

    constructor(private usersService: UsersService,
                private objectsService: ObjectsService,
                private categoriesService: CategoriesService) {
    }

    ngOnInit() {
        Observable.combineLatest(
            this.usersService.getUsers(),
            this.objectsService.getObjects(),
            this.categoriesService.getCategories()
        ).takeUntil(this.destroy$).subscribe((data) => {
            this.allUsers = data[0];
            this.allObjects = data[1];
            this.allCategories = data[2];
            // chart user -> quantity objects
            this.allUsers.forEach((u) => {
                let col = 0;
                this.allObjects.forEach((o) => {
                    if (u.id === o.user) {
                        col += 1;
                    }
                });
                this.dataUser.push({name: u.name, value: col});
            });
            // chart category -> quantity objects
            this.allCategories.forEach((c) => {
                let col2 = 0;
                this.allObjects.forEach((o) => {
                    if (c.id === o.category) {
                        col2 += 1;
                    }
                });
                this.dataCat.push({name: c.name, value: col2});
            });
            this.isLoad = true;
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
