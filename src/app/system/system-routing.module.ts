import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../shared/services/auth-guard.service';

import {SystemComponent} from './system.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {ObjectsListComponent} from './objects-list/objects-list.component';
import {UsersListComponent} from './users-list/users-list.component';
import {SettingsComponent} from './settings/settings.component';
import {ProfileComponent} from './profile/profile.component';

const routes: Routes = [
  {path: '', component: SystemComponent, canActivate: [AuthGuard], children: [
      {path: 'statistics', component: StatisticsComponent},
      {path: 'objects-list', component: ObjectsListComponent},
      {path: 'users-list', component: UsersListComponent},
      {path: 'settings', component: SettingsComponent},
      {path: 'profile', component: ProfileComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {}