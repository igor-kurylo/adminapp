import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message} from '../../../shared/models/message.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/services/users.service';

import {Profile} from '../../shared/models/profile.model';
import {CommonMethodsService} from '../../../shared/services/common-methods.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-edit-data',
    templateUrl: './edit-data.component.html',
    styleUrls: ['./edit-data.component.scss']
})
export class EditDataComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    formFieldsArray = [
        {'field': 'name', 'type': 'text'},
        {'field': 'surname', 'type': 'text'},
        {'field': 'email', 'type': 'email'},
        {'field': 'phone', 'type': 'text'}
    ];
    message: Message;
    userStorageInfo: Profile;
    activeEditSettings: Profile;
    emailDetected = false;

    constructor(private usersService: UsersService,
                private commonMethodsService: CommonMethodsService,
                private formBuilder: FormBuilder) {
    }

    form: FormGroup;

    ngOnInit() {
        this.message = new Message('success', '');
        this.userStorageInfo = JSON.parse(window.sessionStorage.getItem('login'));
        const {name, surname, email, phone, password} = this.userStorageInfo;

        this.activeEditSettings = new Profile(name, surname, email, phone, password);
        this.validateForm(name, surname, email, phone);
    }

    /**
     * Validate form
     * @param {string} name
     * @param {string} surname
     * @param {string} email
     * @param {string} phone
     */
    validateForm(name: string, surname: string, email: string, phone: string): void {
        this.form = this.formBuilder.group({
            'name': [name, [Validators.required]],
            'surname': [surname, [Validators.required]],
            'email': [email, [Validators.required, Validators.email]],
            'phone': [phone, [Validators.required, Validators.maxLength(10), Validators.pattern(/\d{10}/)]]
        });
    }

    /**
     * update user info and update session storage
     */
    onSubmit(): void {
        const {name, surname, email, phone} = this.form.value;
        const editSettings = new Profile(name, surname, email, phone, this.userStorageInfo.password);
        // change detection
        if (JSON.stringify(this.activeEditSettings) !== JSON.stringify(editSettings)) {
            // change email
            this.usersService.getUserByParams(this.userStorageInfo.id, email)
                .map((data) => data[0]).takeUntil(this.destroy$).subscribe(
                (user) => {
                    if (user) {
                        this.message.type = 'danger';
                        this.message.text = 'Such an email exists';
                        this.emailDetected = true;
                    }
                },
                (error) => error,
                () => {
                    if (!this.emailDetected) {
                        // update user info
                        this.usersService.updateUser(this.userStorageInfo.id, editSettings)
                            .takeUntil(this.destroy$).subscribe(() => {
                            this.message.type = 'success';
                            this.message.text = 'Your profile has been edited';
                            this.validateForm(name, surname, email, phone);
                            editSettings.id = this.userStorageInfo.id;
                            window.sessionStorage.clear();
                            window.sessionStorage.setItem('login', JSON.stringify(editSettings));
                            this.usersService.updateUserName(name);
                        });
                    }
                });
            this.activeEditSettings = editSettings;
            this.commonMethodsService.hideMessage(this.message);
            this.emailDetected = false;
        }
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }
}
