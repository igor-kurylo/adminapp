import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../../shared/models/message.model';
import * as md5 from 'md5';
import {UsersService} from '../../shared/services/users.service';
import {Profile} from '../../shared/models/profile.model';
import {CommonMethodsService} from '../../../shared/services/common-methods.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-edit-password',
    templateUrl: './edit-password.component.html',
    styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    message: Message;
    form: FormGroup;
    userStorageInfo: Profile;

    constructor(private usersService: UsersService,
                private commonMethodsService: CommonMethodsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.message = new Message('success', '');

        this.form = this.formBuilder.group({
            'pass': ['', [Validators.required, Validators.minLength(6)]],
            'repeat': ['', [Validators.required, Validators.minLength(6)]]
        });
    }

    /**
     * create new password adn update session storage
     */
    onSubmit(): void {
        this.userStorageInfo = JSON.parse(window.sessionStorage.getItem('login'));
        const {name, surname, email, phone, id} = this.userStorageInfo;
        const pass = md5(this.form.value.pass);
        const editPass = new Profile(name, surname, email, phone, pass);

        this.usersService.updateUser(id, editPass).takeUntil(this.destroy$).subscribe(() => {
            this.message.text = 'password changed';
            this.commonMethodsService.hideMessage(this.message);
            this.form.reset();
            window.sessionStorage.clear();
            editPass.id = id;
            window.sessionStorage.setItem('login', JSON.stringify(editPass));
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
