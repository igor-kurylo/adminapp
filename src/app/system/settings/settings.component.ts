import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Message} from '../../shared/models/message.model';
import {CategoriesService} from '../shared/services/categories.service';
import {CommonMethodsService} from '../../shared/services/common-methods.service';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    form: FormGroup;
    message: Message;

    constructor(private categoriesService: CategoriesService,
                private commonMethodsService: CommonMethodsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.message = new Message('success', '');

        this.form = this.formBuilder.group({
            'category': ['', Validators.required]
        });
    }

    /**
     * create new category
     */
    onSubmit(): void {
        const createCat = {name: this.form.value.category};
        this.message.text = 'Category created';
        this.commonMethodsService.hideMessage(this.message);
        this.categoriesService.createCategory(createCat).takeUntil(this.destroy$)
            .subscribe(() => {
                this.form.reset();
            });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
