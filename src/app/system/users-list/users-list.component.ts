import {Component, OnDestroy, OnInit} from '@angular/core';
import {UsersService} from '../shared/services/users.service';
import {ObjectsService} from '../shared/services/objects.service';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    allUsers;
    allObjects;
    quantityObj = []; // quantity objects of users

    constructor(private usersService: UsersService,
                private objectsService: ObjectsService) {
    }

    ngOnInit() {
        Observable.combineLatest(
            this.usersService.getUsers(),
            this.objectsService.getObjects()
        ).takeUntil(this.destroy$).subscribe((data) => {
            this.allUsers = data[0];
            this.allObjects = data[1];
            // rewrite and composition users list
            this.allUsers.forEach((u) => {
                let col = 0;
                this.allObjects.forEach((o) => {
                    if (u.id === o.user) {
                        col += 1;
                    }
                });
                this.quantityObj.push(col);
            });
        });
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
