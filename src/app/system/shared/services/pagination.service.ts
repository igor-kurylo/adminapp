import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class PaginationService {

    constructor() {}

    totalPages$ = new Subject();
    changePage$ = new Subject();

    /**
     * total page and action
     * @param {{totalPages: number, action: string}} total
     */
    totalPages(total) {
        this.totalPages$.next(total);
    }

    /**
     * change current page
     * @param {number} page
     */
    changePage(page: number) {
        this.changePage$.next(page);
    }
}