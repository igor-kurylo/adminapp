import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {BaseApi} from '../../../shared/core/base-api';

@Injectable()
export class CategoriesService extends BaseApi {

    transferCategories$ = new Subject();

    constructor(public http: HttpClient) {
        super(http);
    }

    /**
     * get categories
     * @returns {Observable<any>}
     */
    getCategories() {
        return this.get('categories');
    }

    /**
     * create category
     * @param data
     * @returns {Observable<any>}
     */
    createCategory(data) {
        return this.post('categories/', data);
    }

    /**
     * transfer categories
     * @param data
     */
    transferCategories(data) {
        this.transferCategories$.next(data);
    }

}