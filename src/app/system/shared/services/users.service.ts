import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {BaseApi} from '../../../shared/core/base-api';

@Injectable()
export class UsersService extends BaseApi {

    updateUserName$ = new Subject();

    constructor(public http: HttpClient) {
        super(http);
    }

    /**
     * get users
     * @returns {Observable<any>}
     */
    getUsers() {
        return this.get('users');
    }

    /**
     * get user by parameters
     * @param {number} id
     * @param {string} email
     * @returns {Observable<any>}
     */
    getUserByParams(id: number, email: string) {
        return this.get(`users?email=${email}&id_ne=${id}`);
    }

    /**
     * update user information
     * @param id
     * @param data
     * @returns {Observable<any>}
     */
    updateUser(id, data) {
        return this.put(`users/${id}`, data);
    }

    /**
     * update user name in header
     * @param name
     */
    updateUserName(name) {
        this.updateUserName$.next(name);
    }

}