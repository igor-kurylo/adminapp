import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import {BaseApi} from '../../../shared/core/base-api';

@Injectable()
export class ObjectsService extends BaseApi {

    afterActionObject$ = new Subject();
    transferEditData$ = new Subject();
    clearEditStyle$ = new Subject();

    constructor(public http: HttpClient) {
        super(http);
    }

    /**
     * get objects
     * @returns {Observable<any>}
     */
    getObjects() {
        return this.get('objects');
    }

    /**
     * delete object
     * @param id
     * @returns {Observable<any>}
     */
    deleteObject(id) {
        return this.delete(`objects/${id}`);
    }

    /**
     * add new object
     * @param data
     * @returns {Observable<any>}
     */
    postObjects(data) {
        return this.post('objects/', data);
    }

    /**
     * update object
     * @param id
     * @param data
     * @returns {Observable<any>}
     */
    putObjects(id, data) {
        return this.put(`objects/${id}`, data);
    }

    /**
     * update object list after action
     * @param added
     */
    afterActionObject(added) {
        this.afterActionObject$.next(added);
    }

    /**
     * transfer data
     * @param data
     */
    transferEditData(data) {
        this.transferEditData$.next(data);
    }

    /**
     * reset style selected object
     * @param val
     */
    clearEditStyle(val) {
        this.clearEditStyle$.next(val);
    }
    
    /**
     * rewrite category and name
     * @param {array} objects
     * @param {array} categories
     * @param {array} users
     */
    rewriteAllObjects(objects, categories, users) {
        objects.forEach((obj) => {
            // rewrite category
            categories.forEach((cat) => {
                if (obj.category === cat.id) {
                    obj.category = cat.name;
                }
            });
            // rewrite id user
            users.forEach((user) => {
                if (obj.user === user.id) {
                    obj.user = user.name;
                }
            });
        });
    }
}