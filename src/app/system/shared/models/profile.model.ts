export class Profile {
    constructor(public name: string,
                public surname: string,
                public email: string,
                public phone: string,
                public password: any,
                public id?: number) {
    }
}