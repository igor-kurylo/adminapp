export class Objects {
    constructor(public category: number,
                public description: string,
                public area: number,
                public price: number,
                public date: string,
                public user: number,
                public id?: number) {
    }
}