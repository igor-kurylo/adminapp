import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UsersService} from '../../services/users.service';
import {AuthService} from '../../../../shared/services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    userName;

    constructor(private router: Router, private usersService: UsersService, private authService: AuthService) {
    }

    ngOnInit() {
        this.userName = JSON.parse(window.sessionStorage.getItem('login'));
        this.usersService.updateUserName$.subscribe((name) => this.userName.name = name);
    }

    /**
     * logOut
     */
    logOut() {
        this.authService.logout();
        this.router.navigate(['/login']);
    }
}
