import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {ObjectsService} from '../../shared/services/objects.service';
import {Objects} from '../../shared/models/objects.model';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-edit-object',
    templateUrl: './edit-object.component.html',
    styleUrls: ['./edit-object.component.scss']
})
export class EditObjectComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    categoriesList; // category list
    theObject;
    isEditShow; // flag show/hide add form
    selectedItem;
    activeEditObj;
    form: FormGroup;

    constructor(private objectsService: ObjectsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.objectsService.transferEditData$.takeUntil(this.destroy$).subscribe((data) => {
            this.categoriesList = data['catList'];
            this.theObject = data['object'];
            this.isEditShow = data['isShow'];

            this.categoriesList.forEach((c) => {
                if (c.name === this.theObject.category) {
                    this.selectedItem = c.id;
                }
            });

            this.form = this.formBuilder.group({
                'editSelect': [this.selectedItem, [Validators.required]],
                'editDescription': [this.theObject.description, [Validators.required, Validators.maxLength(80)]],
                'editArea': [this.theObject.area, [Validators.required, Validators.min(1)]],
                'editPrice': [this.theObject.price, [Validators.required, Validators.min(1)]]
            });

        });
    }

    /**
     * edit object and update session storage
     */
    onSubmit(): void {
        const formData = this.form.value;
        const nowDate = moment(new Date()).format('DD.MM.YYYY');
        const userStorageInfo = JSON.parse(window.sessionStorage.getItem('login'));
        const objectId = this.theObject.id;

        const editObj = new Objects(
            +formData.editSelect,
            formData.editDescription,
            formData.editArea,
            formData.editPrice,
            nowDate,
            userStorageInfo.id
        );
        if (JSON.stringify(this.activeEditObj) !== JSON.stringify(editObj)) {
            this.objectsService.putObjects(objectId, editObj).takeUntil(this.destroy$).subscribe((data) => {
                this.objectsService.afterActionObject(data);
            });
            this.closeEditForm();
        }
        this.activeEditObj = editObj;
    }

    /**
     * close form
     */
    closeEditForm(): void {
        this.isEditShow = false;
        this.objectsService.clearEditStyle(null);
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }
}
