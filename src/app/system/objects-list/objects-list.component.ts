import {Component, OnDestroy, OnInit} from '@angular/core';
import {ObjectsService} from '../shared/services/objects.service';
import {CategoriesService} from '../shared/services/categories.service';
import {UsersService} from '../shared/services/users.service';
import {PaginationService} from '../shared/services/pagination.service';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-objects-list',
    templateUrl: './objects-list.component.html',
    styleUrls: ['./objects-list.component.scss']
})
export class ObjectsListComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    allObjects;
    allCategories;
    allUsers;
    objectsForDelete = [];

    isAddShow = false;
    isEditShow = false;

    activeEditObject;

    showPages;
    currentPage = 1;
    quantityPages = 5;
    action = null;

    constructor(private objectsService: ObjectsService,
                private categoriesService: CategoriesService,
                private usersService: UsersService,
                private paginationService: PaginationService) {
    }

    ngOnInit() {
        this.getObjects();
        this.objectsService.afterActionObject$.takeUntil(this.destroy$).subscribe(() => {
            this.updateObjects('addEdit');
        });
        this.objectsService.clearEditStyle$.takeUntil(this.destroy$).subscribe((val) => {
            this.activeEditObject = val;
        });
        this.paginationService.changePage$.takeUntil(this.destroy$).subscribe((page: number) => {
            this.currentPage = page;
            this.showPages = this.allObjects.slice(this.quantityPages * (this.currentPage - 1),
                this.quantityPages * this.currentPage);

            this.showPages.forEach((i) => {
                i['checked'] = this.objectsForDelete.indexOf(i.id);
            });
        });
    }

    /**
     * get data and reload view
     */
    getObjects() {
        Observable.combineLatest(
            this.objectsService.getObjects(),
            this.categoriesService.getCategories(),
            this.usersService.getUsers()
        ).takeUntil(this.destroy$).subscribe((data) => {
            this.allObjects = data[0];
            this.allCategories = data[1];
            this.allUsers = data[2];
            this.objectsService.rewriteAllObjects(this.allObjects, this.allCategories, this.allUsers);
            this.reloadView('start');
        });
    }

    /**
     * recalculation pages for display and transfer data
     * @param {string} action
     */
    reloadView(action: string) {
        this.showPages = this.allObjects.slice(this.quantityPages * (this.currentPage - 1),
            this.quantityPages * this.currentPage);
        const totalPages = Math.ceil(this.allObjects.length / this.quantityPages);
        this.paginationService.totalPages({totalPages, action: action});
    }

    /**
     * selecting the number of displayed objects
     */
    changeView() {
        this.currentPage = 1;
        this.reloadView('view');
    }

    /**
     * update list after action
     * @param {string} action
     */
    updateObjects(action: string) {
        this.objectsService.getObjects().takeUntil(this.destroy$).subscribe((data) => {
            this.allObjects = data;
            this.objectsService.rewriteAllObjects(this.allObjects, this.allCategories, this.allUsers);
            this.reloadView(action);
        });
    }

    /**
     * delete one object
     * @param {number} id
     */
    deleteObject(id) {
        const res = window.confirm('Object will be deleted');
        if (res) {
            this.objectsService.deleteObject(id).takeUntil(this.destroy$).subscribe(() => {
                this.updateObjects('singleDelete');
            });
        }
    }

    /**
     * delete several objects
     */
    deleteCheckedObjects() {
        const res = window.confirm('Objects will be deleted');
        if (res) {
            const observableArr = [];
            this.objectsForDelete.forEach(id => {
                observableArr.push(this.objectsService.deleteObject(id));
            });
            Observable.concat(...observableArr).subscribe(
                (data) => data,
                (error) => error,
                () => {
                    this.updateObjects('multipleDelete');
                    this.objectsForDelete = [];
                }
            );
        }
    }

    /**
     * list selected items
     * @param {Event} id
     * @return {Array} objectsForDelete
     */
    checkedObject(id) {
        const value = +id.target.value;
        if (id.target.checked) {
            this.objectsForDelete.push(value);
        } else {
            this.objectsForDelete = this.objectsForDelete.filter((item) => {
                return item !== value;
            });
        }
    }

    /**
     * show/hide form + transfer data
     */
    showAddForm() {
        this.isAddShow = true;
        this.activeEditObject = null;
        const data = {
            catList: this.allCategories,
            isShow: this.isAddShow
        };
        this.categoriesService.transferCategories(data);
        // hide form
        if (this.isEditShow) {
            const editData = {object: [], catList: [], isShow: false};
            this.objectsService.transferEditData(editData);
        }
    }

    /**
     * show/hide form + transfer data
     */
    showEditForm(theObject) {
        this.isEditShow = true;
        this.activeEditObject = theObject.id;
        const data = {
            object: theObject,
            catList: this.allCategories,
            isShow: this.isEditShow
        };
        this.objectsService.transferEditData(data);
        // hide form
        if (this.isAddShow) {
            const addData = {catList: [], isShow: false};
            this.categoriesService.transferCategories(addData);
        }
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }

}
