import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoriesService} from '../../shared/services/categories.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ObjectsService} from '../../shared/services/objects.service';
import {Objects} from '../../shared/models/objects.model';
import * as moment from 'moment';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: 'app-add-object',
    templateUrl: './add-object.component.html',
    styleUrls: ['./add-object.component.scss']
})
export class AddObjectComponent implements OnInit, OnDestroy {

    destroy$: Subject<boolean> = new Subject<boolean>();

    categoriesList; // category list
    isAddShow; // flag show/hide add form
    form: FormGroup;

    constructor(private categoriesService: CategoriesService,
                private objectsService: ObjectsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.categoriesService.transferCategories$.takeUntil(this.destroy$).subscribe((data) => {
            this.categoriesList = data['catList'];
            this.isAddShow = data['isShow'];
        });

        this.form = this.formBuilder.group({
            'addSelect': ['', Validators.required],
            'addDescription': ['', [Validators.required, Validators.maxLength(80)]],
            'addArea': ['', [Validators.required, Validators.min(1)]],
            'addPrice': ['', [Validators.required, Validators.min(1)]]
        });

    }

    /**
     * add new object
     */
    onSubmit(): void {
        const formData = this.form.value;
        const nowDate = moment(new Date()).format('DD.MM.YYYY');
        const userStorageInfo = JSON.parse(window.sessionStorage.getItem('login'));
        const createObj = new Objects(
            +formData.addSelect,
            formData.addDescription,
            formData.addArea,
            formData.addPrice,
            nowDate,
            userStorageInfo.id
        );
        this.objectsService.postObjects(createObj).takeUntil(this.destroy$).subscribe((data) => {
            this.form.reset();
            this.objectsService.afterActionObject(data);
        });
    }

    /**
     * close form
     */
    closeAddForm(): void {
        this.isAddShow = false;
    }

    ngOnDestroy() {
        this.destroy$.next(true);
    }
}
