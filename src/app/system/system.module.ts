import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedModule} from '../shared/shared.module';
import {SystemRoutingModule} from './system-routing.module';

import {ObjectsService} from './shared/services/objects.service';
import {UsersService} from './shared/services/users.service';
import {CategoriesService} from './shared/services/categories.service';
import {PaginationService} from './shared/services/pagination.service';
import {AuthGuard} from '../shared/services/auth-guard.service';

import {SystemComponent} from './system.component';
import {HeaderComponent} from './shared/components/header/header.component';
import {SidebarComponent} from './shared/components/sidebar/sidebar.component';
import {StatisticsComponent} from './statistics/statistics.component';
import {ObjectsListComponent} from './objects-list/objects-list.component';
import {UsersListComponent} from './users-list/users-list.component';
import {SettingsComponent} from './settings/settings.component';
import {ProfileComponent} from './profile/profile.component';
import {AddObjectComponent} from './objects-list/add-object/add-object.component';
import {EditObjectComponent} from './objects-list/edit-object/edit-object.component';
import {EditPasswordComponent} from './profile/edit-password/edit-password.component';
import {EditDataComponent} from './profile/edit-data/edit-data.component';
import {PaginationComponent} from './pagination/pagination.component';

@NgModule({
    declarations: [
        SystemComponent,
        HeaderComponent,
        SidebarComponent,
        StatisticsComponent,
        ObjectsListComponent,
        UsersListComponent,
        SettingsComponent,
        ProfileComponent,
        AddObjectComponent,
        EditObjectComponent,
        EditDataComponent,
        EditPasswordComponent,
        PaginationComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        SystemRoutingModule
    ],
    providers: [
        ObjectsService,
        UsersService,
        CategoriesService,
        PaginationService,
        AuthGuard
    ]
})
export class SystemModule {
}
