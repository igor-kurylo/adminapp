import {Component, OnInit} from '@angular/core';
import {PaginationService} from '../shared/services/pagination.service';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

    pages = [];
    pageActive = 1;
    startPage = 1;
    endPage = 7;
    maxLength;

    constructor(private paginationService: PaginationService) {
    }

    ngOnInit() {
        this.paginationService.totalPages$.subscribe(
            data => {
                this.maxLength = data['totalPages'];
                this.maxLength < 7 ? this.endPage = +this.maxLength : this.endPage = 7;

                this.actionDefinition(data);
            }
        );
    }

    /**
     * definition event after action
     * @param {{totalPage: number, action: string}} data
     */
    actionDefinition(data) {
        if (data['action'] === 'start') {
            this.rewritePages(1);
        } else if (data['action'] === 'singleDelete' || data['action'] === 'multipleDelete') {
            if (this.maxLength < this.pageActive) {
                this.getPage(this.maxLength);
            } else {
                this.getPage(this.pageActive);
            }
        } else if (data['action'] === 'view') {
            this.getPage(1);
        } else if (data['action'] === 'addEdit') {
            this.getPage(this.pageActive);
        }
    }

    /**
     * Rewrite array with pagination visible pages
     * @param {Number} page
     */
    rewritePages(page) {
        this.pages = [];
        if (this.maxLength < 7) {
            this.startPage = 1;
            this.endPage = this.maxLength;
        }
        for (let i = this.startPage; i <= this.endPage; i++) {
            this.pages.push(i);
        }
        if (this.maxLength > 7) {
            this.pages[0] = 1;
            this.pages[this.pages.length - 1] = this.maxLength;

            if (page > 3) {
                this.pages[1] = '...';
            }
            if (page < this.maxLength - 2) {
                this.pages[this.pages.length - 2] = '...';
            }
        }
    }

    /**
     * set previous page
     */
    prev() {
        if (this.pageActive !== 1) {
            this.getPage(this.pageActive - 1);
        }
    }

    /**
     * set next page
     */
    next() {
        if (this.pageActive !== this.maxLength) {
            this.getPage(this.pageActive + 1);
        }
    }

    /**
     * definition of pages
     * @param {number} page
     */
    getPage(page) {
        if (typeof page !== 'string') {
            if (page <= 4) {
                this.startPage = 1;
                this.endPage = 7;
                this.rewritePages(page);
            } else if (page + 2 >= this.maxLength) {
                this.startPage = this.maxLength - 6;
                this.endPage = this.maxLength;
                this.rewritePages(page);
            } else {
                this.startPage = page - 3;
                this.endPage = page + 3;
                this.rewritePages(page);
            }
            this.pageActive = page;
            this.paginationService.changePage(page);
        }
    }

}
